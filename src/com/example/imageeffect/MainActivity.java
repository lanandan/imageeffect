package com.example.imageeffect;

import android.support.v7.app.ActionBarActivity;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher.ViewFactory;


public class MainActivity extends Activity implements ViewFactory {
	 
		int imgs[] = 
			{ 
				R.drawable.bank, 
				R.drawable.a1, 
				R.drawable.a2, 
				R.drawable.a3,
				R.drawable.a4,
				R.drawable.a5,
				R.drawable.a6,
				R.drawable.a7,
				R.drawable.a8,
				R.drawable.a9,
				R.drawable.a10,
				R.drawable.a11,
				R.drawable.a12,
				R.drawable.banking
			};
	 
		ImageSwitcher imgSwitcher;
	 
		/** Called when the activity is first created. */
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_main);
	 
			imgSwitcher = (ImageSwitcher) findViewById(R.id.imageSwitcher1);
			imgSwitcher.setFactory(this);
			imgSwitcher.setInAnimation(AnimationUtils.loadAnimation(this,
					android.R.anim.fade_in));
			imgSwitcher.setOutAnimation(AnimationUtils.loadAnimation(this,
					android.R.anim.fade_out));
	 
			Gallery gallery = (Gallery) findViewById(R.id.gallery1);
			gallery.setAdapter(new ImageAdapter(this));
			
			gallery.setOnItemClickListener(new OnItemClickListener() {
	 
				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
						long arg3) {
					imgSwitcher.setImageResource(imgs[arg2]);
				}
			});
	
		}
		@Override
		public View makeView() {
			// TODO Auto-generated method stub
			ImageView iView = new ImageView(this);
			iView.setScaleType(ImageView.ScaleType.FIT_CENTER);
			iView.setLayoutParams(new ImageSwitcher.LayoutParams
				(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
			iView.setBackgroundColor(0xFF000000);
			return iView;
		}
		
		
		public class ImageAdapter extends BaseAdapter {
			 
			private Context ctx;
	 
			public ImageAdapter(Context c) {
				ctx = c; 
			}
	 
			public int getCount() {
	 
				return imgs.length;
			}
	 
			public Object getItem(int arg0) {
	 
				return arg0;
			}
	 
			public long getItemId(int arg0) {
	 
				return arg0;
			}
	 
			public View getView(int arg0, View arg1, ViewGroup arg2) {
	 
				ImageView iView = new ImageView(ctx);
				iView.setImageResource(imgs[arg0]);
				iView.setScaleType(ImageView.ScaleType.FIT_XY);
				iView.setLayoutParams(new Gallery.LayoutParams(200, 150));
				return iView;
			}

		
		}
	 
		
	 
}